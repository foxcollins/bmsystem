<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
})->middleware('guest');

Auth::routes();
Route::get('/logout','Auth\LoginController@logout');
Route::get('/home', 'HomeController@index')->name('home');

//resources Routes
Route::resource('clients','ClientController');
Route::resource('records','RecordController');
Route::resource('phones','PhoneController');

//Individual route to axios
Route::get('/clientsall','ClientController@allClients')->name('allclients');
Route::get('/clientslast','ClientController@lastClients')->name('lastclients');
Route::get('/getclient/{id}','ClientController@getClient')->name('getclient');
Route::get('/getrecords/{id}','RecordController@getRecords')->name('getrecords');
Route::get('/gettags','HomeController@getTags')->name('gettags');
Route::get('/getphones/{id}','PhoneController@getPhones')->name('getphones');
Route::get('/getlastthreads','ThreadController@getLastThreads')->name('getlastthreadss');
Route::get('/sendinfoemail/{id}','ClientController@sendInformationEmail')->name('sendinfo');