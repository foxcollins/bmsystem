<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    //

    /**
         * @inheritdoc
         */
        protected $table = 'taggable_tags';
        /**
         * @inheritdoc
         */
        protected $primaryKey = 'tag_id';

    public function clients()
    {
        return $this->morphedByMany('App\Client', 'taggable_taggables');
    }
    
}
