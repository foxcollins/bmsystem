<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{
    //

    /**
     * Get the client that owns the phone.
     */
    public function client()
    {
        return $this->belongsTo('App\Client');
    }

    /**
         * Get the records for the thread.
         */
    public function records()
    {
        return $this->hasMany('App\Record');
    }
}
