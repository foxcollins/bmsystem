<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    //

    /**
     * Get the client that owns the phone.
     */
    public function client()
    {
        return $this->belongsTo('App\Client');
    }
}
