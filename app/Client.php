<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentTaggable\Taggable;

class Client extends Model
{
    use Taggable;

    /**
         * Get the phones for the client.
         */
    public function phones()
    {
        return $this->hasMany('App\Phone');
    }

    /**
         * Get the phones for the client.
         */
    public function thread()
    {
        return $this->hasOne('App\Thread');
    }

    public function tags()
    {
        return $this->morphToMany('App\Tag', 'taggable', 'taggable_taggables', 'taggable_id', 'tag_id');
    }
    
}
