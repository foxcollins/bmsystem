<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Record extends Model
{
    //

    /**
     * Get the thread that owns the Record.
     */
    public function thread()
    {
        return $this->belongsTo('App\Thread');
    }
}
