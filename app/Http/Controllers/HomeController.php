<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tag;
use App\Client;
use App\Thread;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $clients = Client::get()->count();
        $threads = Thread::where('status','<>','No está interesado')->get()->count();
        $acepts = Thread::where('status','=','Propuesta aceptada')->get()->count();
        $propuestas = Thread::where('status','=','Esperando propuesta')->orWhere('status','=','Formulario recibido')->get()->count();
        return view('home',compact('clients','threads','acepts','propuestas'));
    }

    public function getTags(){
        $data = Tag::orderBy('name','ASC')->get();
        return Response()->Json($data,201);
    }
}
