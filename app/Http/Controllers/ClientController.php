<?php

namespace App\Http\Controllers;

use App\Client;
use App\Phone;
use Illuminate\Http\Request;
use Cviebrock\EloquentTaggable\Taggable;
use Illuminate\Mail\Mailable;
use App\Mail\ServicesInformationMail;
use Mail;
class ClientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**sdsdsds
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //reurn all clients
        $data = Client::with('thread')

        ->get();
        return view('clients.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //mostrar formulario
        return view('clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $messages = [
            'required' => 'Este campo es requerido',
            'unique'   => 'Ya existe y no es posible repetir con otro cliente',
        ];
        $validatedData = $request->validate([
            'brand' => 'required|unique:clients|max:255',
            'email' => 'nullable|unique:clients',
            'phoneid' => 'nullable|unique:phones'
        ],$messages);
        
        //return $request->brand;
       //return Response()->json("hello",201);
        try {

            $client = new Client;
            $client->brand = $request->brand;
            $client->manager = $request->manager;
            $client->email = $request->email;
            $client->address = $request->address;
            $client->instagram = $request->instagram;
            $client->facebook = $request->facebook;
            $client->googlemap = $request->googlemap;
            $client->web = $request->web;
            
            

            if ($client->save()) {
                if ($request->item) {
                    #guardo tags
                    $tags=[];
                    foreach ($request->item as $key => $value) {
                        array_push($tags , $value['text']);
                    }
                    $client->tag($tags);

                }
                if(!empty($request->phoneid)){
                    $phone  = new Phone;
                    $phone->phoneid = $request->phoneid;
                    $phone->type= $request->phonetype;;
                    $phone->client_id = $client->id;
                    $phone->save();  
                }
                $data="Saved";
                return Response()->Json($data,201);
            }
        } catch (\Exception $e) {
            return Response()->Json("ERROR",404);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        //muestro el clients y sus detalles
        return view('clients.show',compact("client"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        //
        return view('clients.edit',compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        //
        $messages = [
            'required' => 'Este campo es requerido',
            'unique'   => 'Ya existe y no es posible repetir con otro cliente',
        ];
        $validatedData = $request->validate([
            'brand' => 'required|max:255|unique:clients,brand,' . $client->id,
            'email' => 'nullable|unique:clients,email,' . $client->id,
        ],$messages);
        $client = Client::findOrFail($client->id);
        $client->brand = $request->brand;
        $client->manager = $request->manager;
        $client->email = $request->email;
        $client->address = $request->address;
        $client->instagram = $request->instagram;
        $client->facebook = $request->facebook;
        $client->googlemap = $request->googlemap;
        $client->web = $request->web;
        if($client->save()){
            if ($request->item) {
                #vacio las tags
                $client->detag();
                $tags=[];
                //recorro el array y lleno tags
                foreach ($request->item as $key => $value) {
                    array_push($tags , $value['text']);
                }
                //registro nuevas tags
                $client->tag($tags);

            }else{
                $client->detag();
            }
            return Response()->Json($client,201);
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        //
    }

    public function sendInformationEmail($id){
        try {
            $client = Client::findOrFail($id);
            Mail::to('llccfox23@gmail.com')->send(new ServicesInformationMail($client));
            return Response()->Json("OK",202);
        } catch (Exception $e) {
            return Response()->Json($e,404);
        }
        
    }

    public function allClients(){
        $data = Client::with('thread')->get();
        return Response()->Json($data,201);
    }
    public function lastClients(){
        $data = Client::orderBy('created_at','DESC')->with('thread')->take(7)->get();
        return Response()->Json($data,201);
    }
    public function getClient($id){
        $data = Client::where('id','=',$id)->with('thread','phones','tags')->firstOrFail();
        return Response()->Json($data,201);
    }
}
