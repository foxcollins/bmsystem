<?php

namespace App\Http\Controllers;

use App\Phone;
use Illuminate\Http\Request;

class PhoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => 'Este campo es requerido',
            'unique'   => 'Ya existe y no es posible repetir con otro cliente',
        ];
        $validatedData = $request->validate([
            'phoneid' => 'required|unique:phones'
        ],$messages);
        try {
            $data = new Phone;
            $data->phoneid = $request->phoneid;
            $data->type = $request->phoneType;
            $data->client_id = $request->clientid;
            if ($data->save()) {
                return Response()->json($data,201);
            }
        } catch (Exception $e) {
            return Response()->json($e,422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Phone  $phone
     * @return \Illuminate\Http\Response
     */
    public function show(Phone $phone)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Phone  $phone
     * @return \Illuminate\Http\Response
     */
    public function edit(Phone $phone)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Phone  $phone
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Phone $phone)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Phone  $phone
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $data = Phone::destroy($id);
        return Response()->Json($data,202);
    }
    public function getPhones($id){
        $data = Phone::where('client_id',$id)->orderBy('type','ASC')->get();
        return Response()->json($data,201);
    }
}
