<?php

namespace App\Http\Controllers;

use App\Record;
use App\Thread;
use Illuminate\Http\Request;

class RecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //return Response()->Json($request->clientId,201);
        try {
            $thread = Thread::where('client_id',$request->clientId)->first();
            if (!$thread) {
                # se crea el thread
                $thread = new Thread;
                $thread->status = $request->status;
                $thread->client_id = $request->clientId;
                $thread->save();
            }
            ////
            $client = new Record;
            $client->status = $request->status;
            $client->observation = $request->observation;
            $client->thread_id = $thread->id;
            if ($client->save()) {
                $thread->status = $request->status;
                $thread->save();
                $data="Saved";
                return Response()->Json($data,201);
            }
        } catch (\Exception $e) {
            return Response()->Json($e,404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Record  $record
     * @return \Illuminate\Http\Response
     */
    public function show(Record $record)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Record  $record
     * @return \Illuminate\Http\Response
     */
    public function edit(Record $record)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Record  $record
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Record $record)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Record  $record
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Record::findOrFail($id);
        $thread_id = $data->thread_id;
        $deleted = Record::destroy($id);
        $count=Record::where('thread_id',$thread_id)->count();
        if ($count>0) {
            # existe otro record y tomaria el thread su status
            $all=Record::where('thread_id',$thread_id)->get();
            $last=$all->last();
            $thread = Thread::findOrFail($thread_id);
            $thread->status = $last->status;
            $thread->save();

        }else{
            //elimino el tread de este cliente para que no tenga status
            $thread = Thread::destroy($thread_id);

        }
        return Response()->Json("RECORD DELETED",201);
    }
    public function  getRecords($id){
        $data = Record::where('thread_id',$id)->orderBy('created_at','DESC')->get();
        return Response()->Json($data,201);
    }
}    
