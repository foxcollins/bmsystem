
require('./bootstrap');

window.Vue = require('vue');
import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import VueSweetalert2 from 'vue-sweetalert2';


Vue.use(VueSweetalert2);

Vue.component('client-list', require('./components/ClientsListComponent.vue').default);
Vue.component('client-all-list', require('./components/ClientsAllListComponent.vue').default);
Vue.component('client-create', require('./components/ClientCreateComponent.vue').default);
Vue.component('client-show-details', require('./components/ClientShowDetailsComponent.vue').default);
Vue.component('client-records-list', require('./components/ClientRecordsComponent.vue').default);
Vue.component('client-edit', require('./components/ClientEditComponent.vue').default);
Vue.component('client-phones', require('./components/ClientPhonesComponent.vue').default);
Vue.component('last-threads', require('./components/LastThreadsComponent.vue').default);


const app = new Vue({
    el: '#app',
});
