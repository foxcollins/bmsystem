@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="row d-flex justify-content-center">
                <div class="col-md-3 m-0">
                    <div class="card mb-0" style="max-width: 540px;">
                      <div class="row no-gutters">
                        <div class="col-md-4 d-flex justify-content-center bg-info">
                          <i class="fas fa-users fa-2x align-self-center text-white"></i>
                        </div>
                        <div class="col-md-8">
                          <div class="card-body d-flex justify-content-center">
                            <h5 class="card-title align-self-center mt-1 mb-0"><b>{{$clients}}</b> Clientes</h5>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
                <div class="col-md-3 m-0">
                    <div class="card mb-0" style="max-width: 540px;">
                      <div class="row no-gutters">
                        <div class="col-md-4 d-flex justify-content-center bg-dark">
                          <i class="fas fa-chart-line fa-2x align-self-center text-white"></i>
                        </div>
                        <div class="col-md-8">
                          <div class="card-body d-flex justify-content-center">
                            <h5 class="card-title align-self-center mt-1 mb-0"><b>{{$threads}}</b> Procecos</h5>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
                <div class="col-md-3 m-0">
                    <div class="card mb-0" style="max-width: 540px;">
                      <div class="row no-gutters">
                        <div class="col-md-4 d-flex justify-content-center bg-warning">
                          <i class="fas fa-chart-line fa-2x align-self-center text-white"></i>
                        </div>
                        <div class="col-md-8">
                          <div class="card-body d-flex justify-content-center">
                            <h5 class="card-title align-self-center mt-1 mb-0"><b>{{$propuestas}}</b> Propuestas</h5>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
                <div class="col-md-3 m-0">
                    <div class="card mb-0" style="max-width: 540px;">
                      <div class="row no-gutters">
                        <div class="col-md-4 d-flex justify-content-center bg-success">
                          <i class="far fa-check-circle fa-2x align-self-center text-white"></i>
                        </div>
                        <div class="col-md-8">
                          <div class="card-body d-flex justify-content-center">
                            <h5 class="card-title align-self-center mt-1 mb-0"><b>{{$acepts}}</b> Aceptados</h5>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-12 mt-5">
            <div class="row">
                <div class="col-md-6">
                    <client-list></client-list>
                </div>
                <div class="col-md-6">
                    <last-threads></last-threads>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
