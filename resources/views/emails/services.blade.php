<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Información de Foxcode</title>
  </head>
<body>
    <div class="text-center">
        <img src="https://ci6.googleusercontent.com/proxy/sAnyz_qJEk7nh7TGU96TokpZI57TllzAuLx4Ou6-ZS-EKXghiT4HhM5HCHi6Nu1l8sqb7Nw7Q_XA_y-2L32vBbJRu_T0YbDSh7r8GqRMH1rXHNVA4Ji02w=s0-d-e1-ft#https://luiscollins.com.ve/wp-content/uploads/2018/12/main_logo-2.png" width="200">
    </div>
    <br>
    <p>Hola {{$saludo}}... 
        @if ($client->manager===null)
            Sres. de <b> {{Str::title($client->brand)}}</b> 
        @else
            <b>{{Str::title($client->manager)}}</b>
        @endif, le escribimos de FoxCode por que estamos interesados en hacer crecer su negocio poniendo a disposición nuestros servicios profesionales de <b>Diseño Web</b> y <b>Marketing Digital</b>, para ello necesitamos por favor que chequee el archivo adjunto con información detallada de nuestros planes y servicios y nos de una pequeña idea de lo que necesita completando nuestro <a href="https://goo.gl/forms/eogd0R2XgkE927gq2">FORMULARIO DE INFORMACIÓN</a> para asi tener una idea mas clara y presentarles una propuesta adecuada para su negocio</p>
    
    <p></p>
    <br>
    <p>Esperando pronta respuesta!...:</p>
    <hr>
</body>
</html>