<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  

  <a class="navbar-brand" href="{{ url('/home') }}">BMSYS</a>
@auth()
  <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item">
        <a class="nav-link" href="{{ url('/home') }}">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('clients.index') }}">Clientes</a>
      </li> 
    </ul>
    <ul class="nav md-justify-content-end mr-5">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle text-white mr-5" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-user-tie"></i> {{auth()->user()->name}}
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="{{ url('/clients/create') }}">Ingresar Cliente</a>
            <a class="dropdown-item" href="#">Another action</a>
            <a class="dropdown-item" href="{{ url('/logout') }}">Salir</a>
          </div>
        </li>
      
      
    </ul>
  </div>
 @else

 @endauth
</nav>
