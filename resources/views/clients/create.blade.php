@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-8">
          <client-create></client-create>
        </div>
    </div>
</div>
@endsection