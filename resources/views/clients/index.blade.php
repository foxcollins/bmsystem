@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-10">
          <client-all-list></client-all-list>
        </div>
    </div>
</div>
@endsection