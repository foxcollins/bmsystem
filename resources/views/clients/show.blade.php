@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row ">

        <div class="col-md-4">
          <client-show-details :clientid="{{$client->id}}"></client-show-details>
        </div>
        <div class="col-md-8">
        	<div class="card">
        		<client-records-list :clientid="{{$client->id}}"></client-records-list>
        	</div>
        </div>
    </div>
</div>
@endsection