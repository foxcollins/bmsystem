-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 15-07-2019 a las 02:23:08
-- Versión del servidor: 5.7.24
-- Versión de PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bmsystem`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clients`
--

CREATE TABLE `clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `brand` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `manager` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `web` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `googlemap` json DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `clients`
--

INSERT INTO `clients` (`id`, `brand`, `manager`, `email`, `address`, `instagram`, `facebook`, `web`, `googlemap`, `created_at`, `updated_at`) VALUES
(1, 'experiencia le coco', NULL, 'lecococreperia@gmail.com', 'Calle Cantuarias 128 - Miraflores Lima', 'https://www.instagram.com/experiencialecoco/', 'https://www.facebook.com/pg/experiencialecoco/about/?ref=page_internal', NULL, '[]', '2019-07-14 22:20:20', '2019-07-14 22:20:20'),
(2, 'sixpackpostres', NULL, NULL, NULL, 'https://www.instagram.com/sixpackpostres/', 'https://www.facebook.com/pg/sixpackpostres/about/?ref=page_internal', NULL, '[]', '2019-07-14 22:22:54', '2019-07-14 22:22:54'),
(3, 'el efecto milagrosa', NULL, 'pedidosmilagrosa@gmail.com', NULL, 'https://www.instagram.com/elefectomilagrosa/', 'https://www.facebook.com/pg/elefectomilagrosa/about/?ref=page_internal', NULL, '[]', '2019-07-14 22:26:13', '2019-07-14 22:26:13'),
(4, 'mis pasiones cafeteria', NULL, NULL, 'Calle la Victoria F-20 / Urb. Sol de Ica ( a la altura de la cuadra 14 de la av. San Martín) Ica', 'https://www.instagram.com/mispasionescafeteria/', 'https://www.facebook.com/pg/MisPasionesica/about/?ref=page_internal', NULL, '[]', '2019-07-14 22:28:11', '2019-07-14 22:28:11'),
(5, 'postres faba', NULL, 'reposteriafabachiappe@gmail.com', NULL, 'https://www.instagram.com/postresfaba/', 'https://www.facebook.com/postresfaba/', NULL, '[]', '2019-07-14 22:30:20', '2019-07-14 22:30:20'),
(6, 'lima lashes', NULL, NULL, NULL, 'https://www.instagram.com/limalashes.pe/', 'https://www.facebook.com/pg/limalashes.sandrayoshimoto/about/?ref=page_internal', NULL, '[]', '2019-07-14 22:33:46', '2019-07-14 22:33:46'),
(7, 'andrea narvaez fotografía', NULL, 'andreanarvaezfotografia@gmail.com', NULL, 'https://www.instagram.com/andreanarvaezfotografia/', 'https://www.facebook.com/pg/andreanarvaezfotografia/about/?ref=page_internal', NULL, '[]', '2019-07-14 22:37:04', '2019-07-14 22:37:04'),
(8, 'el cedro cafe', NULL, NULL, 'Calle Federico Villarreal 022, San Isidro Lima', 'https://www.instagram.com/elcedrocafe/', 'https://www.facebook.com/pg/ElCedroCafe/about/?ref=page_internal', NULL, '[]', '2019-07-14 22:39:23', '2019-07-14 22:39:23'),
(9, 'osadia salon spa', NULL, 'osadia.a@gmail.com', 'AV. Rafael Escardo 599 , San Miguel Lima', 'https://www.instagram.com/osadia.salonspa/', 'https://www.facebook.com/pg/osadiasalonspa/about/?ref=page_internal', NULL, '[]', '2019-07-14 22:41:41', '2019-07-14 22:41:41'),
(10, 'autentica extensiones', NULL, 'autenticaextensiones@gmail.com', 'Jockey Plaza // C.C. El Polo // Conquistadores', 'https://www.instagram.com/autentica.pe/', 'https://www.facebook.com/Autentica-1577205099187720/', NULL, '[]', '2019-07-14 22:57:34', '2019-07-14 22:57:34'),
(11, 'belle forme', NULL, 'belleformeperu@gmail.com', 'Calle Cantuarias 270 Miraflores. Lima', 'https://www.instagram.com/belleformeperu/', 'https://www.facebook.com/pg/Belle.Forme.Peru/about/?ref=page_internal', NULL, '[]', '2019-07-14 22:59:56', '2019-07-14 22:59:56'),
(12, 'sanurya fast good', NULL, 'cantuarias01@sanurya.com', 'Calle Cantuarias 160 Miraflores Lima  /  Av. Primavera 611 San Borja  /  Patio de comidas del Jockey Plaza', NULL, 'https://www.facebook.com/pg/Brow-Plus--1009923619197393/about/?ref=page_internal', 'http://sanuryafastgood.com/?reqp=1&reqr=nzcdYaAhpUWiLzW4YaOvrt==', '[]', '2019-07-14 23:17:22', '2019-07-14 23:17:22'),
(13, 'intimarket', NULL, 'intimarket.mg@gmail.com', 'Jr. Camaná 355 Lima', 'https://www.instagram.com/intimarket_pe/', 'https://www.facebook.com/pg/intimarket/about/?ref=page_internal', NULL, '[]', '2019-07-14 23:19:54', '2019-07-14 23:19:54'),
(14, 'glamour salon lounge spa', NULL, NULL, 'Calle Preciados Nro 175 Tienda 5B Óvalo de Higuereta, Surco Lima', 'https://www.instagram.com/glamoursalonloungespa/', 'https://www.facebook.com/glamoursalonloungespa/?ref=br_rs', NULL, '[]', '2019-07-14 23:26:03', '2019-07-14 23:26:03'),
(15, 'las divas oficial', NULL, 'divasrous@gmail.com', 'Av. Chimu 778 2do Piso Zarate San Juan De Lurigancho, Lima, Peru', 'https://www.instagram.com/lasdivas.oficial/', 'https://www.facebook.com/pg/Las.DivasOficiall/about/?ref=page_internal', NULL, '[]', '2019-07-14 23:29:46', '2019-07-14 23:29:46'),
(16, 'waykuna', NULL, 'waykuna.pe@gmail.com', NULL, 'https://www.instagram.com/waykuna.peru/', 'https://www.facebook.com/pg/peru.waykuna/about/?ref=page_internal', NULL, '[]', '2019-07-14 23:34:17', '2019-07-14 23:34:17'),
(17, 'punto organico', NULL, 'biotienda@puntoorganico.com', 'Av.Mariscal La Mar 718 Miraflores', 'https://www.instagram.com/puntoorganico/', 'https://www.facebook.com/pg/punto.organico.9/about/?ref=page_internal', 'http://www.puntoorganico.com', '[]', '2019-07-14 23:39:44', '2019-07-14 23:39:44'),
(18, 'samaca organico', NULL, 'pedidos@samacaorganico.pe', 'Av. Tejada 510 (altura de la cuadra 12 de la av 28 de julio de Miraflores) Lima', 'https://www.instagram.com/samaca.organico/', 'https://www.facebook.com/pg/samaca.organico/about/?ref=page_internal', 'http://www.samacaorganico.pe', '[]', '2019-07-14 23:43:37', '2019-07-14 23:43:37'),
(19, 'laddy perfect', NULL, 'laddy_mar@hotmail.com', 'Miraflores & Pueblo libre Lima', 'https://www.instagram.com/laddy.perfect/', 'https://www.facebook.com/pg/cejasperfectaslima/about/?ref=page_internal', NULL, '[]', '2019-07-14 23:46:57', '2019-07-14 23:46:57'),
(20, 'catalulashes', NULL, 'catalulashes@gmail.com', 'Surco:AV TOMAS MARSANO 3767  /  Los olivos:AV CARLOS IZAGUIRRE 438', 'https://www.instagram.com/catalulashes_/', 'https://www.facebook.com/pg/Catal%C3%BA-sal%C3%B3n-247151109317493/about/?ref=page_internal', NULL, '[]', '2019-07-14 23:47:18', '2019-07-14 23:50:29'),
(21, 'lartigiana', NULL, 'dellemilia.ph@gmail.com', 'Vasco nuñez de balboa 751 miraflores Lima', 'https://www.instagram.com/lartigiana_byalexiegarcia/', 'https://www.facebook.com/pg/byalexiegarcia/about/?ref=page_internal', NULL, '[]', '2019-07-14 23:54:54', '2019-07-14 23:54:54'),
(22, 'pasito a pasos store', NULL, 'pasitoapaso11@gmail.com', NULL, 'https://www.instagram.com/pasitoapasostore/', 'https://m.facebook.com/pg/pasitoapasostore/about/?ref=page_internal&mt_nav=0', NULL, '[]', '2019-07-14 23:58:39', '2019-07-14 23:58:39'),
(23, 'qallaliy', NULL, 'qallaliydesign@gmail.com', NULL, 'https://www.instagram.com/qallaliy_peru/', 'https://www.facebook.com/pg/Qallaliy.peru/about/?ref=page_internal', NULL, '[]', '2019-07-15 00:00:55', '2019-07-15 00:00:55'),
(24, 'anelare', NULL, NULL, NULL, 'https://www.instagram.com/anelare/', 'https://www.facebook.com/pg/anelarehelados/about/?ref=page_internal', NULL, '[]', '2019-07-15 00:03:07', '2019-07-15 00:03:07'),
(25, 'bombicis bebés', NULL, 'info@bombicis.com', 'Av Angamos Oeste 1122 Miraflores', 'https://www.instagram.com/bombicisbebes/', 'https://www.facebook.com/ropadebebebombicis/', 'https://bombicis.com.pe/', '[]', '2019-07-15 00:05:55', '2019-07-15 00:05:55'),
(26, 'pesca mira', 'oscar miano', 'pescamira@icloud.com', NULL, 'https://www.instagram.com/pescamira/', 'https://www.facebook.com/Pescamira-2290456244554536/', NULL, '[]', '2019-07-15 00:08:01', '2019-07-15 00:08:01'),
(27, 'carnes del gordo', NULL, 'contacto.delgordo@gmail.com', NULL, 'https://www.instagram.com/carnes.delgordo/', 'https://www.facebook.com/Del-Gordo-Carnes-Premium-1832862433460530/?ref=br_rs', NULL, '[]', '2019-07-15 00:11:19', '2019-07-15 00:11:19'),
(28, 'la mamita pasteleria', NULL, NULL, NULL, 'https://www.instagram.com/lamamitapasteleria/', 'https://www.facebook.com/LaMamitaPasteleria/', NULL, '[]', '2019-07-15 00:26:36', '2019-07-15 00:26:36'),
(29, 'Mona Mia atelier', NULL, 'monamia.atelier@gmail.com', NULL, 'https://www.instagram.com/monamia_atelier/', 'https://www.facebook.com/monamia.atelier/', NULL, '[]', '2019-07-15 00:28:04', '2019-07-15 00:28:04'),
(30, 'lavifood', NULL, NULL, 'Guardia Civil, Guardia Peruana Mercado Santa Rosa Chorrillos', 'https://www.instagram.com/lavifood/', 'https://www.facebook.com/LaviFood-404409343229224/', NULL, '[]', '2019-07-15 00:30:31', '2019-07-15 00:30:31'),
(31, 'packery', NULL, 'packery.co@gmail.com', NULL, 'https://www.instagram.com/packery.co/', 'https://www.facebook.com/Packery-213851785886130/', NULL, '[]', '2019-07-15 00:33:21', '2019-07-15 00:35:43'),
(32, 'amai', NULL, NULL, NULL, 'https://www.instagram.com/amaiperu/', 'https://www.facebook.com/amaiheladosartesanales/', NULL, '[]', '2019-07-15 00:40:35', '2019-07-15 00:40:35'),
(33, 'alma fria paletas', NULL, 'almafriapaleteria1@gmail.com', 'Calle Martínez de Compañon 333 Trujillo  /  Av. Las Palmeras 3854, Los Olivos, Lima', 'https://www.instagram.com/almafriapaletas/', 'https://www.facebook.com/almafriapaleteria/', NULL, '[]', '2019-07-15 00:44:01', '2019-07-15 00:44:01'),
(34, 'buenos hábitos nutrición', 'nutricionista carola navarro', 'mcarolanavarro@gmail.com', 'av. Javier Prado Este  476, Lima', 'https://www.instagram.com/buenoshabitosnutricion/', 'https://www.facebook.com/pg/buenoshabitosnutricion/', NULL, '[]', '2019-07-15 00:48:44', '2019-07-15 00:48:44'),
(35, 'postres miski', NULL, NULL, 'Surco Lima', 'https://www.instagram.com/postres_miski/', 'https://www.facebook.com/Miskipasion/', NULL, '[]', '2019-07-15 01:03:07', '2019-07-15 01:03:07'),
(36, 'appletree taller infantil', NULL, NULL, 'La Molina-Lima, Peru', 'https://www.instagram.com/appletree_taller_infantil/', NULL, NULL, '[]', '2019-07-15 01:14:14', '2019-07-15 01:14:14'),
(37, 'lea fort', 'lea fort', 'leafort.shoes@gmail.com', 'Av Conquistadores 447 Lima', 'https://www.instagram.com/leafort.shoes/', 'https://www.facebook.com/leafort.shoes/', NULL, '[]', '2019-07-15 01:18:41', '2019-07-15 01:18:41'),
(38, 'bocatto catering', NULL, 'bocattocateringyeventos@hotmail.es', 'trujillo', 'https://www.instagram.com/bocattocatering/', 'https://www.facebook.com/bocattocateringtru/', NULL, '[]', '2019-07-15 01:29:58', '2019-07-15 01:29:58'),
(39, 'el jardin de xime', NULL, 'eljardindexime@gmail.com', NULL, 'https://www.instagram.com/eljardindexime/', 'https://www.facebook.com/eljardindexime/', NULL, '[]', '2019-07-15 01:34:49', '2019-07-15 01:34:49'),
(40, 'cupcakes now', 'eli', 'e.cupcakes@gmail.com', 'la molina, lima', 'https://www.instagram.com/cupcakes.now/', 'https://www.facebook.com/cupcakesnowcoli/?ref=br_rs', NULL, '[]', '2019-07-15 01:40:06', '2019-07-15 01:40:06'),
(41, 'khipu art', NULL, 'yosselyn.c92@gmail.com', NULL, 'https://www.instagram.com/khipu_art/', 'https://www.facebook.com/Khipu.Peru/', NULL, '[]', '2019-07-15 01:42:24', '2019-07-15 01:42:24'),
(42, 'lima bonita', NULL, NULL, 'Av. Santa Cruz 814, Miraflores Ovalo Gutiérrez 2do piso del CC. Alcazar', 'https://www.instagram.com/lima_bonita/', 'https://www.facebook.com/limabonitastore/?tn-str=k*F', NULL, '[]', '2019-07-15 01:48:16', '2019-07-15 01:48:16'),
(43, 'flamingo studio', NULL, NULL, 'Jiron Mariscal Luzuriaga 442, Jesús María 15072 Galería Shopping Way, Tienda 20 (9,51 km) 11 Lima', 'https://www.instagram.com/flamingostudio.pe/', 'https://www.facebook.com/flamingostudio.pe', NULL, '[]', '2019-07-15 01:52:52', '2019-07-15 01:52:52'),
(44, 'alice pasteleria', NULL, 'aliciat2105@gmail.com', NULL, 'https://www.instagram.com/alicepasteleria1/', 'https://www.facebook.com/alicepasteleria1/', NULL, '[]', '2019-07-15 01:57:34', '2019-07-15 01:57:34'),
(45, 'frenchies lima', 'alice', NULL, 'Alberto del campo San Isidro Lima', 'https://www.instagram.com/frenchies_lima/', 'https://www.facebook.com/frenchieslima/', NULL, '[]', '2019-07-15 02:01:32', '2019-07-15 02:01:32'),
(46, 'maoli', NULL, NULL, NULL, 'https://www.instagram.com/maoli_artisan_soap/', 'https://www.facebook.com/pg/maolimx/', NULL, '[]', '2019-07-15 02:12:00', '2019-07-15 02:12:00'),
(47, 'producciones farrou\'s', NULL, 'ofuna23@outlook.es', 'la Molina vieja Lima', NULL, 'https://www.facebook.com/Producciones-Farrous-212629269492237/', NULL, '[]', '2019-07-15 02:14:43', '2019-07-15 02:14:43'),
(48, 'angelis deco', NULL, NULL, NULL, NULL, 'https://www.facebook.com/angelisdeco/', NULL, '[]', '2019-07-15 02:16:01', '2019-07-15 02:16:01'),
(49, 'fausta pasteleria', NULL, NULL, 'Calle General Mendiburu 738, Miraflores Lima', 'https://www.instagram.com/faustapasteleria/', 'https://www.facebook.com/fausta.pasteleria/', NULL, '[]', '2019-07-15 02:17:33', '2019-07-15 02:17:33'),
(50, 'la patisserie escuela', 'Gia Danielle Gavancho', 'escuela.lapatisserie@gmail.com', NULL, 'https://www.instagram.com/lapatisserie.escuela/', 'https://www.facebook.com/LaPatisserieEscuela/', NULL, '[]', '2019-07-15 02:25:18', '2019-07-15 02:25:18'),
(51, 'escuela peruana del café', NULL, NULL, 'Jirón Emilio Fernández 390 A urb. Santa Beatriz Lima', 'https://www.instagram.com/escuelaperuanadelcafe/', 'https://www.facebook.com/escuelaperuanadelcafe/', NULL, '[]', '2019-07-15 02:28:11', '2019-07-15 02:28:11'),
(52, 'la critica rosshhcaaa', NULL, 'hermescgsa@hotmail.com', NULL, 'https://www.instagram.com/lacriticarosshhcaaa/', 'https://www.facebook.com/criticarosshhcaaaalexoviang/', 'http://www.cosacom.com', '[]', '2019-07-15 02:32:04', '2019-07-15 02:32:04'),
(53, 'the cherry look', NULL, 'thecherrylook@gmail.com', NULL, 'https://www.instagram.com/thecherrylook/', 'https://www.facebook.com/cherrystudio.pe/', NULL, '[]', '2019-07-15 02:37:55', '2019-07-15 02:37:55'),
(54, 'la otra pichanga', 'lorena cetraro', 'laotrapichanga@gmail.com', NULL, 'https://www.instagram.com/laotrapichanga/', 'https://www.facebook.com/laotrapichanga/', NULL, '[]', '2019-07-15 02:42:08', '2019-07-15 02:42:08'),
(55, 'nutrición barf peru', NULL, NULL, NULL, 'https://www.instagram.com/nutricionbarfperu/', 'https://www.facebook.com/nutricionbarfperu/?ref=br_rs', NULL, '[]', '2019-07-15 02:44:31', '2019-07-15 02:44:31'),
(56, 'vacheria sandra jarufe', 'sandra jarufe', 'vacheria.pasteleria@gmail.com', NULL, 'https://www.instagram.com/vacheria_sandrajarufe/', 'https://www.facebook.com/vacheria14/?__tn__=%2Cd%2CP-R&eid=ARDDkcj1evdkSnJfFekCb4d69i1G_a4lnSQO4DcWWFpA2Hp5xL9LLiOiDMWqsr8g7C9xBBRjXxHH53qN', NULL, '[]', '2019-07-15 02:47:36', '2019-07-15 02:47:36'),
(57, 'dra zoila cubas', NULL, NULL, 'av. rio marañon 688 Los Olivos', 'https://www.instagram.com/drazoilacubas/', 'https://www.facebook.com/drazoilacubas/?__tn__=%2Cd%2CP-R&eid=ARDip0rGz7Q_9YhU3QMz-iChGTLG3VDRGDiCFyso7KaVRpHWzskkG6PZak14iVvJ670A3YojijrOLpAj', NULL, '[]', '2019-07-15 03:20:24', '2019-07-15 03:24:05'),
(58, 'baja de peso', 'Evi Quiñones', NULL, NULL, 'https://www.instagram.com/bajadepeso_peru/', 'https://www.facebook.com/bajadepesoperuoficial/', NULL, '[]', '2019-07-15 03:50:04', '2019-07-15 03:50:04'),
(59, 'sueño de pastel', 'Yanina Guzmán', 'suenodepastelperu@gmail.com', NULL, 'https://www.instagram.com/suenodepastel_/', 'https://www.facebook.com/suenodepastelperu/', NULL, '[]', '2019-07-15 03:52:00', '2019-07-15 03:52:00'),
(60, 'dulce xplosao', NULL, NULL, NULL, 'https://www.instagram.com/dulcexplosao/', 'https://www.facebook.com/dulcexplosao/', NULL, '[]', '2019-07-15 03:55:36', '2019-07-15 03:55:36'),
(61, 'zoraida nutricionista', NULL, NULL, NULL, 'https://www.instagram.com/zoraida_nutricionista/', 'https://www.facebook.com/zoraida.cabrera.104', NULL, '[]', '2019-07-15 03:59:14', '2019-07-15 03:59:14'),
(62, 'ale guilbert makeup', NULL, 'aleguibertmakeup@gmail.com', NULL, NULL, 'https://www.facebook.com/aleguibertmakeupartist/', NULL, '[]', '2019-07-15 04:01:28', '2019-07-15 04:01:28'),
(63, 'cucharita y cucharon', NULL, 'catering.cucharitaycucharon@gmail.com', NULL, 'https://www.instagram.com/cucharita_y_cucharon/', 'https://www.facebook.com/CucharitaYCucharonPeru/', NULL, '[]', '2019-07-15 04:05:18', '2019-07-15 04:05:18'),
(64, 'la bocana pasteleria', NULL, 'hola@labocana.pe', NULL, 'https://www.instagram.com/labocanapasteleria/', 'https://www.facebook.com/pg/LaBocanaPasteleria/', NULL, '[]', '2019-07-15 04:15:33', '2019-07-15 04:15:33'),
(65, 'el zorro thomas', NULL, 'gurbina@grupoholz.com', 'Andrés Reyes 144, San Isidro', 'https://www.instagram.com/el_zorrothomas/', 'https://www.facebook.com/zorroThomas/', NULL, '[]', '2019-07-15 04:22:55', '2019-07-15 04:22:55'),
(66, 'la aromatica', NULL, NULL, NULL, 'https://www.instagram.com/la.aromatica1/', 'https://www.facebook.com/la.aromatica1/', NULL, '[]', '2019-07-15 04:25:49', '2019-07-15 04:25:49');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '0000_00_00_000000_create_taggable_table', 1),
(2, '2014_10_12_000000_create_users_table', 1),
(3, '2014_10_12_100000_create_password_resets_table', 1),
(4, '2019_07_05_213419_create_clients_table', 1),
(5, '2019_07_05_213508_create_phones_table', 1),
(6, '2019_07_05_214859_create_threads_table', 1),
(7, '2019_07_05_215102_create_records_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `phones`
--

CREATE TABLE `phones` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `phoneid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `phones`
--

INSERT INTO `phones` (`id`, `phoneid`, `type`, `client_id`, `created_at`, `updated_at`) VALUES
(1, '982 402 755', 'landline', 2, '2019-07-14 22:22:54', '2019-07-14 22:22:54'),
(2, '(056) 386690', 'landline', 4, '2019-07-14 22:28:12', '2019-07-14 22:28:12'),
(3, '992545592', 'whatsapp', 5, '2019-07-14 22:30:20', '2019-07-14 22:30:20'),
(4, '998870809', 'whatsapp', 5, '2019-07-14 22:31:23', '2019-07-14 22:31:23'),
(5, '960 311 626', 'whatsapp', 6, '2019-07-14 22:33:47', '2019-07-14 22:33:47'),
(6, '959467524', 'whatsapp', 8, '2019-07-14 22:39:23', '2019-07-14 22:39:23'),
(7, '(01) 5619744', 'landline', 9, '2019-07-14 22:41:41', '2019-07-14 22:41:41'),
(8, '977 597 124', 'whatsapp', 10, '2019-07-14 22:57:34', '2019-07-14 22:57:34'),
(9, '922 200 040', 'whatsapp', 11, '2019-07-14 22:59:56', '2019-07-14 22:59:56'),
(10, '993 001 862', 'whatsapp', 12, '2019-07-14 23:17:22', '2019-07-14 23:17:22'),
(11, '922 716 430', 'whatsapp', 13, '2019-07-14 23:19:54', '2019-07-14 23:19:54'),
(12, '6581541', 'landline', 14, '2019-07-14 23:26:04', '2019-07-14 23:26:04'),
(13, '998278601', 'whatsapp', 14, '2019-07-14 23:26:31', '2019-07-14 23:26:31'),
(14, '922555940', 'whatsapp', 14, '2019-07-14 23:26:48', '2019-07-14 23:26:48'),
(15, '994 315 923', 'whatsapp', 15, '2019-07-14 23:29:47', '2019-07-14 23:29:47'),
(16, '7238746', 'landline', 15, '2019-07-14 23:30:52', '2019-07-14 23:30:52'),
(17, '7221546', 'landline', 15, '2019-07-14 23:31:08', '2019-07-14 23:31:08'),
(18, '916417254', 'whatsapp', 16, '2019-07-14 23:34:17', '2019-07-14 23:34:17'),
(19, '(01) 2210966', 'landline', 17, '2019-07-14 23:39:44', '2019-07-14 23:39:44'),
(20, '3406361', 'landline', 18, '2019-07-14 23:43:37', '2019-07-14 23:43:37'),
(21, '935 541 539', 'whatsapp', 19, '2019-07-14 23:46:57', '2019-07-14 23:46:57'),
(22, '912 730 073', 'whatsapp', 20, '2019-07-14 23:51:29', '2019-07-14 23:51:29'),
(23, '961 617 047', 'whatsapp', 21, '2019-07-14 23:54:54', '2019-07-14 23:54:54'),
(24, '997287732', 'landline', 25, '2019-07-15 00:05:55', '2019-07-15 00:05:55'),
(25, '913019647', 'whatsapp', 26, '2019-07-15 00:08:01', '2019-07-15 00:08:01'),
(26, '920252114', 'whatsapp', 27, '2019-07-15 00:11:20', '2019-07-15 00:11:20'),
(27, '991 076 386', 'whatsapp', 29, '2019-07-15 00:28:05', '2019-07-15 00:28:05'),
(28, '936 203 506', 'whatsapp', 30, '2019-07-15 00:30:32', '2019-07-15 00:30:32'),
(29, '923336365', 'whatsapp', 31, '2019-07-15 00:33:21', '2019-07-15 00:33:21'),
(30, '989861533', 'whatsapp', 32, '2019-07-15 00:40:36', '2019-07-15 00:40:36'),
(31, '993 752 643', 'whatsapp', 33, '2019-07-15 00:44:02', '2019-07-15 00:44:02'),
(32, '952 323 023', 'whatsapp', 35, '2019-07-15 01:03:07', '2019-07-15 01:03:07'),
(33, '987774043', 'whatsapp', 37, '2019-07-15 01:18:42', '2019-07-15 01:18:42'),
(34, '982515015', 'whatsapp', 38, '2019-07-15 01:29:58', '2019-07-15 01:29:58'),
(35, '944220266', 'whatsapp', 38, '2019-07-15 01:31:17', '2019-07-15 01:31:17'),
(36, '989 036 627', 'whatsapp', 40, '2019-07-15 01:40:06', '2019-07-15 01:40:06'),
(37, '912595639', 'whatsapp', 41, '2019-07-15 01:42:24', '2019-07-15 01:42:24'),
(38, '977730478', 'whatsapp', 42, '2019-07-15 01:48:16', '2019-07-15 01:48:16'),
(39, '(01) 5059229', 'landline', 42, '2019-07-15 01:48:54', '2019-07-15 01:48:54'),
(40, '924 090 914', 'whatsapp', 43, '2019-07-15 01:52:52', '2019-07-15 01:52:52'),
(41, '972725514', 'whatsapp', 44, '2019-07-15 01:57:34', '2019-07-15 01:57:34'),
(42, '944 554 741', 'whatsapp', 45, '2019-07-15 02:01:32', '2019-07-15 02:01:32'),
(43, '939 273 778', 'whatsapp', 47, '2019-07-15 02:14:44', '2019-07-15 02:14:44'),
(44, '995 053 431', 'whatsapp', 48, '2019-07-15 02:16:01', '2019-07-15 02:16:01'),
(45, '989 496 359', 'whatsapp', 49, '2019-07-15 02:17:33', '2019-07-15 02:17:33'),
(46, '997646471', 'whatsapp', 50, '2019-07-15 02:25:18', '2019-07-15 02:25:18'),
(47, '(01) 457-6410', 'landline', 50, '2019-07-15 02:25:55', '2019-07-15 02:25:55'),
(48, '941 481 939', 'whatsapp', 51, '2019-07-15 02:28:11', '2019-07-15 02:28:11'),
(49, '997 932 251', 'whatsapp', 52, '2019-07-15 02:32:04', '2019-07-15 02:32:04'),
(50, '943172830', 'whatsapp', 53, '2019-07-15 02:37:55', '2019-07-15 02:37:55'),
(51, '955003357', 'whatsapp', 54, '2019-07-15 02:42:08', '2019-07-15 02:42:08'),
(52, '933680029', 'whatsapp', 55, '2019-07-15 02:44:31', '2019-07-15 02:44:31'),
(53, '987763003', 'whatsapp', 56, '2019-07-15 02:47:36', '2019-07-15 02:47:36'),
(54, '995750548', 'whatsapp', 57, '2019-07-15 03:20:24', '2019-07-15 03:20:24'),
(55, '970 550 512', 'whatsapp', 57, '2019-07-15 03:32:11', '2019-07-15 03:32:11'),
(56, '989281823', 'whatsapp', 58, '2019-07-15 03:50:04', '2019-07-15 03:50:04'),
(57, '961748236', 'whatsapp', 59, '2019-07-15 03:52:00', '2019-07-15 03:52:00'),
(58, '957555400', 'whatsapp', 60, '2019-07-15 03:55:36', '2019-07-15 03:55:36'),
(59, '993 396 388', 'whatsapp', 62, '2019-07-15 04:01:29', '2019-07-15 04:01:29'),
(60, '991769080', 'whatsapp', 63, '2019-07-15 04:05:18', '2019-07-15 04:05:18'),
(61, '940668952', 'whatsapp', 63, '2019-07-15 04:07:09', '2019-07-15 04:07:09'),
(62, '956596374', 'whatsapp', 65, '2019-07-15 04:22:55', '2019-07-15 04:22:55'),
(63, '936 123 888', 'whatsapp', 66, '2019-07-15 04:25:49', '2019-07-15 04:25:49');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `records`
--

CREATE TABLE `records` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `observation` text COLLATE utf8mb4_unicode_ci,
  `thread_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `taggable_taggables`
--

CREATE TABLE `taggable_taggables` (
  `tag_id` int(10) UNSIGNED NOT NULL,
  `taggable_id` int(10) UNSIGNED NOT NULL,
  `taggable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `taggable_taggables`
--

INSERT INTO `taggable_taggables` (`tag_id`, `taggable_id`, `taggable_type`, `created_at`, `updated_at`) VALUES
(1, 1, 'App\\Client', NULL, NULL),
(2, 2, 'App\\Client', NULL, NULL),
(3, 4, 'App\\Client', NULL, NULL),
(2, 5, 'App\\Client', NULL, NULL),
(4, 6, 'App\\Client', NULL, NULL),
(5, 6, 'App\\Client', NULL, NULL),
(6, 6, 'App\\Client', NULL, NULL),
(7, 7, 'App\\Client', NULL, NULL),
(3, 8, 'App\\Client', NULL, NULL),
(8, 8, 'App\\Client', NULL, NULL),
(9, 9, 'App\\Client', NULL, NULL),
(10, 9, 'App\\Client', NULL, NULL),
(4, 9, 'App\\Client', NULL, NULL),
(5, 10, 'App\\Client', NULL, NULL),
(4, 10, 'App\\Client', NULL, NULL),
(4, 11, 'App\\Client', NULL, NULL),
(11, 11, 'App\\Client', NULL, NULL),
(12, 11, 'App\\Client', NULL, NULL),
(8, 12, 'App\\Client', NULL, NULL),
(13, 13, 'App\\Client', NULL, NULL),
(14, 13, 'App\\Client', NULL, NULL),
(15, 13, 'App\\Client', NULL, NULL),
(9, 14, 'App\\Client', NULL, NULL),
(10, 14, 'App\\Client', NULL, NULL),
(16, 15, 'App\\Client', NULL, NULL),
(11, 15, 'App\\Client', NULL, NULL),
(17, 15, 'App\\Client', NULL, NULL),
(18, 16, 'App\\Client', NULL, NULL),
(19, 16, 'App\\Client', NULL, NULL),
(14, 17, 'App\\Client', NULL, NULL),
(15, 17, 'App\\Client', NULL, NULL),
(13, 18, 'App\\Client', NULL, NULL),
(20, 18, 'App\\Client', NULL, NULL),
(17, 19, 'App\\Client', NULL, NULL),
(5, 19, 'App\\Client', NULL, NULL),
(10, 19, 'App\\Client', NULL, NULL),
(4, 19, 'App\\Client', NULL, NULL),
(17, 20, 'App\\Client', NULL, NULL),
(5, 20, 'App\\Client', NULL, NULL),
(21, 20, 'App\\Client', NULL, NULL),
(22, 21, 'App\\Client', NULL, NULL),
(23, 22, 'App\\Client', NULL, NULL),
(24, 22, 'App\\Client', NULL, NULL),
(14, 22, 'App\\Client', NULL, NULL),
(25, 23, 'App\\Client', NULL, NULL),
(26, 23, 'App\\Client', NULL, NULL),
(22, 24, 'App\\Client', NULL, NULL),
(27, 24, 'App\\Client', NULL, NULL),
(28, 25, 'App\\Client', NULL, NULL),
(24, 25, 'App\\Client', NULL, NULL),
(8, 26, 'App\\Client', NULL, NULL),
(29, 27, 'App\\Client', NULL, NULL),
(30, 27, 'App\\Client', NULL, NULL),
(28, 29, 'App\\Client', NULL, NULL),
(24, 29, 'App\\Client', NULL, NULL),
(31, 30, 'App\\Client', NULL, NULL),
(32, 30, 'App\\Client', NULL, NULL),
(2, 31, 'App\\Client', NULL, NULL),
(22, 32, 'App\\Client', NULL, NULL),
(27, 32, 'App\\Client', NULL, NULL),
(22, 33, 'App\\Client', NULL, NULL),
(27, 33, 'App\\Client', NULL, NULL),
(1, 34, 'App\\Client', NULL, NULL),
(33, 34, 'App\\Client', NULL, NULL),
(34, 34, 'App\\Client', NULL, NULL),
(2, 35, 'App\\Client', NULL, NULL),
(35, 36, 'App\\Client', NULL, NULL),
(25, 37, 'App\\Client', NULL, NULL),
(2, 38, 'App\\Client', NULL, NULL),
(36, 39, 'App\\Client', NULL, NULL),
(37, 39, 'App\\Client', NULL, NULL),
(2, 40, 'App\\Client', NULL, NULL),
(36, 41, 'App\\Client', NULL, NULL),
(38, 41, 'App\\Client', NULL, NULL),
(28, 42, 'App\\Client', NULL, NULL),
(39, 42, 'App\\Client', NULL, NULL),
(21, 43, 'App\\Client', NULL, NULL),
(4, 43, 'App\\Client', NULL, NULL),
(2, 44, 'App\\Client', NULL, NULL),
(2, 45, 'App\\Client', NULL, NULL),
(40, 45, 'App\\Client', NULL, NULL),
(41, 46, 'App\\Client', NULL, NULL),
(27, 46, 'App\\Client', NULL, NULL),
(30, 47, 'App\\Client', NULL, NULL),
(30, 48, 'App\\Client', NULL, NULL),
(2, 49, 'App\\Client', NULL, NULL),
(20, 51, 'App\\Client', NULL, NULL),
(35, 51, 'App\\Client', NULL, NULL),
(2, 50, 'App\\Client', NULL, NULL),
(7, 53, 'App\\Client', NULL, NULL),
(1, 54, 'App\\Client', NULL, NULL),
(42, 54, 'App\\Client', NULL, NULL),
(14, 55, 'App\\Client', NULL, NULL),
(43, 55, 'App\\Client', NULL, NULL),
(2, 56, 'App\\Client', NULL, NULL),
(4, 58, 'App\\Client', NULL, NULL),
(34, 58, 'App\\Client', NULL, NULL),
(2, 59, 'App\\Client', NULL, NULL),
(2, 60, 'App\\Client', NULL, NULL),
(1, 61, 'App\\Client', NULL, NULL),
(33, 61, 'App\\Client', NULL, NULL),
(34, 61, 'App\\Client', NULL, NULL),
(44, 62, 'App\\Client', NULL, NULL),
(4, 62, 'App\\Client', NULL, NULL),
(30, 63, 'App\\Client', NULL, NULL),
(2, 63, 'App\\Client', NULL, NULL),
(2, 64, 'App\\Client', NULL, NULL),
(46, 66, 'App\\Client', NULL, NULL),
(47, 66, 'App\\Client', NULL, NULL),
(24, 65, 'App\\Client', NULL, NULL),
(45, 65, 'App\\Client', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `taggable_tags`
--

CREATE TABLE `taggable_tags` (
  `tag_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `normalized` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `taggable_tags`
--

INSERT INTO `taggable_tags` (`tag_id`, `name`, `normalized`, `created_at`, `updated_at`) VALUES
(1, 'comida', 'comida', '2019-07-14 22:20:20', '2019-07-14 22:20:20'),
(2, 'postres', 'postres', '2019-07-14 22:22:54', '2019-07-14 22:22:54'),
(3, 'cafeteria', 'cafeteria', '2019-07-14 22:28:12', '2019-07-14 22:28:12'),
(4, 'belleza', 'belleza', '2019-07-14 22:33:46', '2019-07-14 22:33:46'),
(5, 'pestañas', 'pestañas', '2019-07-14 22:33:47', '2019-07-14 22:33:47'),
(6, 'cejas', 'cejas', '2019-07-14 22:33:47', '2019-07-14 22:33:47'),
(7, 'fotografia', 'fotografia', '2019-07-14 22:37:04', '2019-07-14 22:37:04'),
(8, 'restaurante', 'restaurante', '2019-07-14 22:39:23', '2019-07-14 22:39:23'),
(9, 'salon', 'salon', '2019-07-14 22:41:41', '2019-07-14 22:41:41'),
(10, 'spa', 'spa', '2019-07-14 22:41:41', '2019-07-14 22:41:41'),
(11, 'estetica', 'estetica', '2019-07-14 22:59:56', '2019-07-14 22:59:56'),
(12, 'cirugia', 'cirugia', '2019-07-14 22:59:56', '2019-07-14 22:59:56'),
(13, 'tienda', 'tienda', '2019-07-14 23:19:54', '2019-07-14 23:19:54'),
(14, 'alimentos', 'alimentos', '2019-07-14 23:19:54', '2019-07-14 23:19:54'),
(15, 'organicos', 'organicos', '2019-07-14 23:19:54', '2019-07-14 23:19:54'),
(16, 'antiedad', 'antiedad', '2019-07-14 23:29:46', '2019-07-14 23:29:46'),
(17, 'microblading', 'microblading', '2019-07-14 23:29:47', '2019-07-14 23:29:47'),
(18, 'sandwicheria', 'sandwicheria', '2019-07-14 23:34:17', '2019-07-14 23:34:17'),
(19, 'domicilio', 'domicilio', '2019-07-14 23:34:17', '2019-07-14 23:34:17'),
(20, 'cafe', 'cafe', '2019-07-14 23:43:37', '2019-07-14 23:43:37'),
(21, 'uñas', 'uñas', '2019-07-14 23:50:29', '2019-07-14 23:50:29'),
(22, 'heladeria', 'heladeria', '2019-07-14 23:54:54', '2019-07-14 23:54:54'),
(23, 'articulos', 'articulos', '2019-07-14 23:58:39', '2019-07-14 23:58:39'),
(24, 'bebés', 'bebés', '2019-07-14 23:58:39', '2019-07-14 23:58:39'),
(25, 'zapatos', 'zapatos', '2019-07-15 00:00:55', '2019-07-15 00:00:55'),
(26, 'handmade', 'handmade', '2019-07-15 00:00:55', '2019-07-15 00:00:55'),
(27, 'artesanal', 'artesanal', '2019-07-15 00:03:08', '2019-07-15 00:03:08'),
(28, 'ropa', 'ropa', '2019-07-15 00:05:55', '2019-07-15 00:05:55'),
(29, 'carnes', 'carnes', '2019-07-15 00:11:19', '2019-07-15 00:11:19'),
(30, 'catering', 'catering', '2019-07-15 00:11:20', '2019-07-15 00:11:20'),
(31, 'comidas', 'comidas', '2019-07-15 00:30:31', '2019-07-15 00:30:31'),
(32, 'yogurt', 'yogurt', '2019-07-15 00:30:31', '2019-07-15 00:30:31'),
(33, 'nutricion', 'nutricion', '2019-07-15 00:48:45', '2019-07-15 00:48:45'),
(34, 'salud', 'salud', '2019-07-15 00:48:45', '2019-07-15 00:48:45'),
(35, 'educación', 'educación', '2019-07-15 01:14:14', '2019-07-15 01:14:14'),
(36, 'deco', 'deco', '2019-07-15 01:34:49', '2019-07-15 01:34:49'),
(37, 'jardin', 'jardin', '2019-07-15 01:34:50', '2019-07-15 01:34:50'),
(38, 'interiores', 'interiores', '2019-07-15 01:42:24', '2019-07-15 01:42:24'),
(39, 'femenina', 'femenina', '2019-07-15 01:48:16', '2019-07-15 01:48:16'),
(40, 'vegano', 'vegano', '2019-07-15 02:01:32', '2019-07-15 02:01:32'),
(41, 'jabon', 'jabon', '2019-07-15 02:12:00', '2019-07-15 02:12:00'),
(42, 'pichanga', 'pichanga', '2019-07-15 02:42:08', '2019-07-15 02:42:08'),
(43, 'animales', 'animales', '2019-07-15 02:44:31', '2019-07-15 02:44:31'),
(44, 'makeup', 'makeup', '2019-07-15 04:01:28', '2019-07-15 04:01:28'),
(45, 'muebles', 'muebles', '2019-07-15 04:22:55', '2019-07-15 04:22:55'),
(46, 'difusor', 'difusor', '2019-07-15 04:25:49', '2019-07-15 04:25:49'),
(47, 'aromaterapia', 'aromaterapia', '2019-07-15 04:25:49', '2019-07-15 04:25:49');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `threads`
--

CREATE TABLE `threads` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'llccfox23@gmail.com', '2019-07-14 21:58:40', '$2y$10$Xh3X1lqXcK7Iy7tLolMRGuuL/8zh9o0tjhfV6Gh2FuXsr64thnfha', NULL, NULL, NULL),
(2, 'Maria Madrid', 'mariamadrid990@gmail.com', '2019-07-14 05:00:00', '$2y$10$Xh3X1lqXcK7Iy7tLolMRGuuL/8zh9o0tjhfV6Gh2FuXsr64thnfha', NULL, '2019-07-14 05:00:00', '2019-07-14 05:00:00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `clients_brand_unique` (`brand`),
  ADD UNIQUE KEY `clients_email_unique` (`email`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `phones`
--
ALTER TABLE `phones`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `phones_phoneid_unique` (`phoneid`),
  ADD KEY `phones_client_id_foreign` (`client_id`);

--
-- Indices de la tabla `records`
--
ALTER TABLE `records`
  ADD PRIMARY KEY (`id`),
  ADD KEY `records_thread_id_foreign` (`thread_id`);

--
-- Indices de la tabla `taggable_taggables`
--
ALTER TABLE `taggable_taggables`
  ADD KEY `i_taggable_fwd` (`tag_id`,`taggable_id`),
  ADD KEY `i_taggable_rev` (`taggable_id`,`tag_id`),
  ADD KEY `i_taggable_type` (`taggable_type`);

--
-- Indices de la tabla `taggable_tags`
--
ALTER TABLE `taggable_tags`
  ADD PRIMARY KEY (`tag_id`),
  ADD KEY `taggable_tags_normalized_index` (`normalized`);

--
-- Indices de la tabla `threads`
--
ALTER TABLE `threads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `threads_client_id_foreign` (`client_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clients`
--
ALTER TABLE `clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `phones`
--
ALTER TABLE `phones`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT de la tabla `records`
--
ALTER TABLE `records`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `taggable_tags`
--
ALTER TABLE `taggable_tags`
  MODIFY `tag_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT de la tabla `threads`
--
ALTER TABLE `threads`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `phones`
--
ALTER TABLE `phones`
  ADD CONSTRAINT `phones_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `records`
--
ALTER TABLE `records`
  ADD CONSTRAINT `records_thread_id_foreign` FOREIGN KEY (`thread_id`) REFERENCES `threads` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `threads`
--
ALTER TABLE `threads`
  ADD CONSTRAINT `threads_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
