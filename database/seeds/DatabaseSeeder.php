<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        DB::table('users')->insert([
        	'name'  => 'admin',
           	'email' => 'llccfox23@gmail.com',
           	'email_verified_at'=>Carbon::now(),
           	'password' => bcrypt('12345678')
       ]);
    }
}
